(function (require, module) {

    var args = require("yargs")
               .usage("Usage: node index.js [Options]")
               .demand("o")
               .alias("o", "output-folder")
               .describe("o", "Path to output FOLDER")
               .demand("s")
               .alias("s", "size")
               .describe("s", "Size of parts in MBs")                              
               .help("h")
               .alias("h", "help")
               .epilog("Copyright 2016 Vikas Gautam")
               .argv

   var $ = require("Streamify")
   var _ = require("transforms")
   var format = require("util").format
   var fs = require("fs")

   var destFolder = args.o
   var splitSize = args.s * 1024 * 1024

   process.stdin
      .pipe($(_.readline))
      .pipe($(main))

   function main(inputFile, enc, getNextFile) {
      var stream = this
      fs.createReadStream(inputFile, "UTF-8")
        .pipe($(_.readline))
        .on("data", distribute)
        .on("finish", getNextFile)

      function distribute(line) {
         if (stream.dataLength === undefined) { stream.dataLength = 0 }
         stream.dataLength += (line.length + 1)

         var tempFileNum = parseInt(Math.floor(stream.dataLength/splitSize))
         if (stream.fileNum !== tempFileNum) {
            stream.fileNum = tempFileNum
            var outputFile = format("%s%d", destFolder, stream.fileNum)
            console.error(outputFile)
            stream.outputStream = fs.createWriteStream(outputFile)
         }

         stream.outputStream.write(line + "\n")
      }      
   }


})(require, module)